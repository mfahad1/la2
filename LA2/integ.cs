﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LA2
{
    class integ
    {
        public ArrayList integ1(string intg)
        {
         
            bool myFloat = false;

            int[,] idArr = new int[,]
            {
                {3,1},
                {3,2},
                {2,2},
                {3,2}
            };

            int currSt = 0, i = 0;
            int strLen = intg.Length;
            ArrayList ArrL = new ArrayList();
            while (i < strLen)
            {

                if ((intg[i] >= 48 && intg[i] <= 57))
                    currSt = idArr[currSt, 0];

                else if (intg[i] == 43 || intg[i] == 45)
                    currSt = idArr[currSt, 1];
                else if (intg[i] == '.' || intg[i] == 'e')
                {
                    myFloat = true;
                    break;
                }

                else if (i > 0)
                {
                    ArrL.Add("FALSE");

                    return ArrL;
                }
                else return null;           
                i++;
            }
           

                if (currSt == 3 && myFloat == false)
                {
                    ArrL.Add("TRUE");
                    ArrL.Add("INTEGER");
                    return ArrL;
                }

                else if (myFloat == true)
                {
                    flt fl = new flt();
                    if (fl.flt1(intg))
                    {
                        ArrL.Add("TRUE");
                        ArrL.Add("FLOAT");
                        return ArrL;
                    }else
                    {
                        ArrL.Add("FALSE");

                        return ArrL;
                    }

                }
            

            return null;
        }

    }

    class flt
    {
        public bool flt1(string myFl)
        {
            int[,] idArr = new int[,]
            {
                {3,1,2,8},
                {3,8,2,8},
                {3,8,2,5},
                {8,8,4,8},
                {8,8,4,5},
                {8,7,6,8},
                {8,8,6,8},
                {8,8,6,8},
                {8,8,8,8}
            };

            int currSt = 0, i = 0;
            int strLen = myFl.Length;
            currSt = 0;

            while (i < strLen)
            {

                if ((myFl[i] == '.'))
                    currSt = idArr[currSt, 0];
                else if ((myFl[i] == '+' || myFl[i] == '-'))
                    currSt = idArr[currSt, 1];

                else if ((myFl[i] >= 48 && myFl[i] <= 57))
                {

                    currSt = idArr[currSt, 2];
                }
                else if (myFl[i] == 'e')
                    currSt = idArr[currSt, 3];

                i++;
            }




            if (currSt == 4 || currSt == 6)
            {
                return true;
            }
            return false;
        }
    }
}
