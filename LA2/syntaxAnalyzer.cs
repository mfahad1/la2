﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LA2
{
    class syntaxAnalyzer
    {
        ArrayList token = new ArrayList();
        int couter = 0;
        string[] NT1stSET = { "blueprint ", "virtual", "override", "static ", "public", "private", "protected", "sealed" };
        string[] blueP_1stNTSET = { "blueprint", "virtual", "override", "static", "public", "private", "protected", "sealed", "blueprint", "$", "SourceCode" };
        string[] accessModSET = { "blueprint", "public", "private", "protected", "sealed", "int", "float", "string", "char", "func" };
        string[] VOSET = { "blueprint", "virtual", "override", "static", "public", "private", " protected", "sealed", "int", "float", "string", "char", "func" };
        string[] staticedSET = { "blueprint", "static", "public", "private", "protected", "sealed", "int", "float", "string", "char", "func" };
        string[] bluePSET = { "virtual", "override", "static", "public", "private", "protected", "sealed", "blueprint" };
        string[] starterSET = { "starter" };
        string[] classBodySET = { "virtual", "override", "static", "public", "private", "protected",
                                 "sealed", "int", "float", "string", "char", "func", "initializer","func","}","starter" };
        string[] LF_classBodySET = { "int", "float", "string", "char", "func" };
        string[] constructorSET = { "initializer" };
        string[] DTSET = { "int", "float", "char", "string" };
        string[] funcSET = { "func" };
        string[] returnedSET = { "int", "float", "char", "string", "long", "void" };
        string[] retuningSET = { "return" };
        string[] def_paramSET = { ",", "{" };
        string[] def_param1SET = { "int", "float", "char", "string" };
        string[] s_stSET = { "int", "float", "char", "string", "ID", "since", "a2z", "agar", "INC", "DEC" };
        string[] m_stSET = { "ID", "since", "a2z", "agar", "INC", "DEC", "}","return","exit" };
        string[] LF_s_stSET = { ":", "INC", "DEC" };
        string[] LF1_s_stSET = { ".=", "=" };
        string[] sinceSET = { "since" };
        string[] func_callingSET = { "ID" };
        string[] paramSET = { "int", "float", "char", "string", "ID", "int_const", "float_const", "string_const", "char_const" };
        string[] LF_paramSET = { "ID", "const" };
        string[] LF2_paramSET = { ",", "+", "-", "*", "/", ":" };
        string[] param2SET = { "," };
        string[] DecSET = { "int", "float", "char", "string" };
        string[] LISTedSET = { ";", ",", "virtual", "override", "static", "public", "private", "protected", "sealed", "int", "float", "string", "char", "func", "initializer", "}", ":" };

        string[] InitSET = { "=", ";", ",", "ID", "since", "a2z", "agar", "INC", "DEC", "}", "aurAgar ", "virtual", "override", "static", "public", "private", "protected", "sealed", "int", "float", "char", "string", "func", "initializer", ":", "%", "/", "*" };
        string[] LF_InitSET = { "ID", "int_const", "float_const", "string_const", "char_const" };
        string[] or_eSET = { "ID", "NOT", "const", "(", "INC", "DEC", "int", " float", "char", "string" };
        string[] and_eSET = { "ID", "NOT", "const", "( ", "INC ", "DEC", "int ", "float", "char", "string" };
        string[] or_e_pSET = { "OR" };
        string[] and_e_pSET = { "AND" };
        string[] R_ESET = { "ID", "NOT", "const", "( ", "INC ", "DEC", "int ", "float", "char", "string" };
        string[] R_E_pSET = { "+=", "-=", "*=", "==", "<=", ">=", "<", ">" };
        string[] ROPSET = { "+=", "-=", "*=", "==", "<=", ">=", "<", ">" };
        string[] ESET = { "ID", "NOT", "const", "(", "INC", "DEC", "int", " float", "char", "string" };
        string[] E_pSET = { "+", "-" };
        string[] PMSET = { "+", "-" };
        string[] TSET = { "ID", "NOT", "const", "(", "INC", "DEC", "int", " float", "char", "string" };
        string[] T_pSET = { "%", "/", "*" };
        string[] M_DMSET = { "%", "/", "*" };
        string[] LF_F1SET = { "%", "/", "*", "ID" };
        string[] LF_FSET = { ":", "%", "/", "*", "ID", "INC", "DEC", "}", ":", "ID", "int", "float", "char", "string", "since", "a2z", "agar", "INC", "DEC", "{", ")", "+=", "-=", "*=", "==", "<=", ">=", "<", ">", "AND", "OR" };
        string[] FSET = { "ID", "NOT", "const", "(", "INC", "DEC", "int", " float", "char", "string" };
        string[] AOPSET = { "+=", "-=", "*=" };
        string[] Agar_condSET = { "ID", "NOT", "const", "(", "INC", "DEC", "int", " float", "char", "string" };
        string[] aurAgarSET = { "ID", "since", "a2z", "agar", "INC", "DEC", "}", "aurAgar" };
        string[] agarSET = { "agar" };
        string[] INCSET = { "ID ", "INC", "DEC" };
        string[] arraySET = { "int", "float", "char", "string" };
        string[] LF_arraySET = { "(", "integer_constant", "float_constant", "string_constant", "char_constant" };
        string[] ar2dSET = { "}", "," };
        string[] array2d_bodySET = { "(" };
        string[] LF_ar_TSET = { "integer_constant", "float_constant", "string_constant", "char_constant", "," };
        string[] ar_TSET = { "(" };
        string[] array2dSET = { "int", "float", "char", "string" };
        string[] Ar2SET = { "}", "," };
        string[] ArSET = { "integer_constant", "float_constant", "string_constant", "char_constant", "}" };
        string[] array1dSET = { "int", "float", "char", "string" };
        string[] for2SET = { ",", "ID", "int ", "float", "char", "string", "since", "a2z", "agar", "INC ", "DEC", "{" };
        string[] bodySET = { "ID", "int ", "float", "char", "string", "since", "a2z", "agar", "INC ", "DEC", "{" };
        string[] LF_for2SET = { "+", "-", "*", "/", "INC", "DEC" };
        string[] LF_for1SET = { "ID", "integer_constant", "float_constant", "string_constant", "char_constant" };
        string[] for1SET = { "ID", "INC", "DEC" };
        string[] or_e_a2zSET = { ":", "ID", "NOT", "const", "(", "INC", "DEC", "int ", "float ", "char", "string" };
        string[] dec_a2zSET = { ":", "int ", "float", "char", "string" };
        string[] a2zSET = { "a2z" };
        string[] E1SET = { "+", "-", "}", ":", "ID", "int", "float", "char", "string", "since", "a2z", "agar", "INC", "DEC", "{", ")", "+=", "-=", "*=", "==", "<=", ">=", "<", ">", "AND", "OR" };
        string[] T1SET = { "%", "/", "*", "}", ":", "ID", "int", "float", "char", "string", "since", "a2z", "agar", "INC", "DEC", "{", ")", "+=", "-=", "*=", "==", "<=", ">=", "<", ">", "AND", "OR" };
        string[] R_E1SET = { "+", "-", "}", ":", "ID", "int", "float", "char", "string", "since", "a2z", "agar", "INC", "DEC", "{", ")", "+=", "-=", "*=", "==", "<=", ">=", "<", ">", "AND", "OR" };
        string[] LF_INCSET = { "INC", "DEC" };
        string[] LF1_InitSET = { "=", ":", ";", "ID", "since", "a2z", "agar", "INC", "DEC", "}", "aurAgar", "virtual  ", "override", "static ", "public ", "private ", "protected", "sealed", "int", "float", "string", "char", "func", "initializer  ", "func", "}", ": ", "%", "/", "*" };
        string[] and_e1SET = { "AND", "}", ":", "ID", "int", "float", "char", "string", "since", "a2z", "agar", "INC", "DEC", "{", ")", "+=", "-=", "*=", "==", "<=", ">=", "<", ">", "AND", "OR" };
        string[] or_e1SET = { "OR","}" , ":",  "ID","int", "float","char","string","since","a2z","agar","INC","DEC","{",")","+=","-=","*=","==","<=",">=","<",">","AND","OR"};
        string[] forAirthSET = {"+","-","*","/",";",",","ID","since","a2z","agar","INC","DEC","}","aurAgar","virtual","override","static","public","private","protected","sealed","int"
                                   ,"float","string","char","initializer","func","}",":","%","/","*"};
        public syntaxAnalyzer(ArrayList al)
        {
            token = al;
        }
        ArrayList onlyClassPart = new ArrayList();
        public void display()
        {
            foreach (string[,] dsa in token)
            {
                //Console.WriteLine(dsa[0, 0] +" class "+dsa[0,1]);
               // if (int.Parse(dsa[0, 1]) >= int.Parse("0") && int.Parse(dsa[0, 1]) <= int.Parse("1"))
                if (dsa[0, 1] == "STRING" || dsa[0, 1] == "char" || dsa[0, 1] == "INTEGER" || dsa[0, 1] == "FLOAT")
                {
                    if (dsa[0, 1] == "STRING")
                        onlyClassPart.Add("string_constant");
                     if (dsa[0, 1] == "char")
                        onlyClassPart.Add("char_constant");
                     if (dsa[0, 1]== "INTEGER")
                        onlyClassPart.Add("int_constant");
                    if (dsa[0, 1] == "FLOAT")
                        onlyClassPart.Add("float_constant");
                }else
                    if (dsa[0, 1] != "ID" && dsa[0, 0] !="")
                    onlyClassPart.Add(dsa[0, 0]);
                else
                    onlyClassPart.Add("ID");
         
            }
           
            foreach (string dsa in onlyClassPart)
            {
                
                Console.WriteLine("classPart        "+dsa);
                
            }
            onlyClassPart.Add("$");
        }

        public bool FNT()
        {
            if (blueP_1stNT())
            {

                
                    if (onlyClassPart[couter].ToString() == "blueprint")
                    {
                        couter++;
                    if (onlyClassPart[couter].ToString() == "SourceCode")
                    {
                        couter++;
                        if (onlyClassPart[couter].ToString() == "{")
                        {
                            couter++;
                            if (classBody())
                            {
                                if (starter())
                                {
                                    if (classBody())
                                    {
                                        if (onlyClassPart[couter].ToString() == "}")
                                        {
                                            couter++;
                                            couter++;
                                            if (blueP_1stNT())
                                            {
                                                return true;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        
                    }
                }
            }
            couter++;
            if (onlyClassPart[couter].ToString() == '$'.ToString()) return true;

            return false;
        }

        public bool blueP_1stNT()
        {
            if (blueP())
            {
                return true;
            }
            else
            {
                foreach(string s in blueP_1stNTSET)
                {
                    if (s == onlyClassPart[couter].ToString())
                    {
                        couter--;
                        return true;
                    }
                }
            }
             

            return false;
        }

        public bool blueP()
        {
            if (vo())
            {
                if (staticed())
                {
                    if (AM())
                    {
                        if (onlyClassPart[couter].ToString() == "blueprint")
                        {
                            couter++;
                            if (onlyClassPart[couter].ToString() == "ID")
                            {
                                couter++;
                                if (onlyClassPart[couter].ToString() == "{")
                                {
                                    couter++;
                                    if(classBody())
                                    {
                                        couter++;
                                        if (onlyClassPart[couter].ToString() == "}")
                                        {
                                            return true;
                                        }
                                    }
                                    
                                }
                            }
                            else
                            {
                              
                                return false;
                            }
                        }
                    }
                }
            }


            return false;
        }

        public bool classBody()
        {
            if (vo())
            {
                if (staticed())
                {
                    if (AM())
                    {
                        if (LF_classBody())
                        {
                            return true;
                        }
                    }
                }
            }
             if (constructor())
            {
                if (classBody())
                {
                    return true;
                }
            }
            else
            {
                foreach (string s in classBodySET)
                {
                    if (s == onlyClassPart[couter].ToString())
                    {

                        couter--;
                        return true;
                    }
                }
            }

            return false;
        }

        public bool LF_classBody()
        {
            if(Dec()||func())
            {
                if(classBody())
                {
                    return true;
                }
            }

            return false;
        }

        public bool func()
        {
            if (onlyClassPart[couter].ToString() == "func")
            {
                couter++;
                if (onlyClassPart[couter].ToString() == "ID")
                {
                    couter++;
                    if (onlyClassPart[couter].ToString() == ":")
                    {
                        couter++;
                        if (returner())
                        {
                            if(def_param())
                            {
                                if (onlyClassPart[couter].ToString() == "{")
                                {
                                    couter++;
                                    if(m_st())
                                    {
                                        if(retuning())
                                        {
                                            if (onlyClassPart[couter].ToString() == "}")
                                            {
                                                couter++;
                                                return true;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    
                    return false;
                }
            }

            return false;
        }

        public bool retuning()
        {
            if (onlyClassPart[couter].ToString() == "return")
            {
                couter++;
                if (retrunVoid())
                {
                    
                    return true;
                }
            }

            return false;
        }

        public bool retrunVoid()
        {
            if (onlyClassPart[couter].ToString() == "ID" || onlyClassPart[couter].ToString() == "void")
            {
                couter++;
                return true;
            }
            return false;
        }


        public bool vo()
        {

            if (onlyClassPart[couter].ToString() == "virtual" || onlyClassPart[couter].ToString() == "override")
            {
                couter++;
                return true;
            }

            else
            {
                foreach (string s in VOSET)
                {
                    if (s == onlyClassPart[couter].ToString())
                        return true;
                }
            }
            return false;
        }

        public bool staticed()
        {
            if (onlyClassPart[couter].ToString() == "static")
            {
                couter++;
                return true;
            }
            else
            {
                foreach (string s in staticedSET)
                {
                    if (s == onlyClassPart[couter].ToString())
                        return true;
                }
            }
            return false;
        }

        public bool AM()
        {
            if (onlyClassPart[couter].ToString() == "public"||onlyClassPart[couter].ToString() =="private"||onlyClassPart[couter].ToString() =="protected"||onlyClassPart[couter].ToString() =="sealed")
            {
                couter++;
                return true;
            }
            else
            {
                foreach (string s in accessModSET)
                {
                    if (s == onlyClassPart[couter].ToString())
                        return true;
                }
            }

            return false;
        }

        public bool constructor()
        {
            if (onlyClassPart[couter].ToString() == "initializer")
            {
                couter++;
                if (onlyClassPart[couter].ToString() == ":")
                {
                    couter++;
                    if(def_param1())
                    {
                        if (onlyClassPart[couter].ToString() == "{")
                        {
                            couter++;
                            if(m_st())
                            {
                                if (onlyClassPart[couter].ToString() == "}")
                                {
                                    couter++;
                                    return true;
                                }
                            }
                        }
                    }

                    
                }
            }

            return false;
        }

        public bool def_param()
        {
            if (onlyClassPart[couter].ToString() == ",")
            {
                couter++;
                if (def_param1())
                {
                    return true;
                }
            }
            else
            {
                foreach (string s in def_paramSET)
                {
                    if (s == onlyClassPart[couter].ToString())
                    {
                        
                        return true;
                    }
                }
            }


            return false;
        }

        public bool def_param1()
        {
            if(DT())
            {
                if (onlyClassPart[couter].ToString() == "ID")
                {
                    couter++;
                    if (def_param())
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public bool starter()
        {
            if (onlyClassPart[couter].ToString() == "func")
            {
                couter++;
                if (onlyClassPart[couter].ToString() == "starter")
                {
                    couter++;
                    if (onlyClassPart[couter].ToString() == ":")
                    {
                        couter++;
                        if (returner())
                        {
                            
                                if (def_param())
                                {
                                    if (onlyClassPart[couter].ToString() == "{")
                                    {
                                        couter++;
                                        if (m_st())
                                        {
                                            if (retuning())
                                            {
                                                if (onlyClassPart[couter].ToString() == "}")
                                                {
                                                    couter++;
                                                    return true;
                                                }
                                            }
                                        }
                                       
                                    }
                                
                            }

                        }
                    }
                }
            }

            return false;
        }

        public bool returner()
        {
            if (DT() )
            {

                return true;
            }
            if(onlyClassPart[couter].ToString() == "void"){
                couter++;
                return true;
            }

            return false;
        }

        public bool m_st()
        {
            if (s_st())
            {
                if (m_st())
                {
                    return true;
                }
            }
            else
            {
                foreach (string s in m_stSET)
                {
                    if (s == onlyClassPart[couter].ToString())
                        return true;
                }
            }
             return false;
        }

        public bool s_st()
        {
            if (onlyClassPart[couter].ToString() == "ID")
            {
                couter++;
                if (LF_s_st())
                {
                    return true;
                }
            }

            if (DT())
            {
                if (onlyClassPart[couter].ToString() == "ID")
                {
                    couter++;
                    if (LF1_s_st())
                    {
                        return true;
                    }
                }
            }

            if (since())
            {
                return true;
            }
            if (a2z())
            {
                return true;
            }


            if (agar())
            {
                return true;
            }
            if (onlyClassPart[couter].ToString() == "INC")
            {
                couter++;
                if (onlyClassPart[couter].ToString() == "ID")
                {
                    couter++;
                    return true;
                }
            }
            if (onlyClassPart[couter].ToString() == "DEC")
            {
                couter++;
                if (onlyClassPart[couter].ToString() == "ID")
                {
                    couter++;
                    return true;
                }
            }
            return false;
        }

        public bool LF1_s_st()
        {
            if (Init())
            {
                if (LISTed())
                {
                    return true;
                }
            }

            if (onlyClassPart[couter].ToString() == ".=")
            {
                couter++;
                if (onlyClassPart[couter].ToString() == "{")
                {
                    couter++;
                    if(LF_array())
                    {
                        return true;
                    }

                }
            }

            return false;
        }

        public bool since()
        {
            if (onlyClassPart[couter].ToString() == "since")
            {
                couter++;
                if (onlyClassPart[couter].ToString() == "{")
                {
                    couter++;
                    if (m_st())
                    {
                        if (onlyClassPart[couter].ToString() == "exit")
                        {
                            couter++;
                            if (onlyClassPart[couter].ToString() == ":")
                            {
                                couter++;
                                if (or_e())
                                {
                                    if (onlyClassPart[couter].ToString() == "}")
                                    {
                                        couter++;
                                        return true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return false;
        }


        public bool a2z()
        {
            if (onlyClassPart[couter].ToString() == "a2z")
            {
                couter++;
                if (onlyClassPart[couter].ToString() == ":")
                {
                    couter++;
                    if (dec_a2z())
                    {
                        if (onlyClassPart[couter].ToString() == ":")
                        {
                            couter++;
                            if (or_e_a2z())
                            {
                                if (onlyClassPart[couter].ToString() == ":")
                                {
                                    couter++;
                                    if (for1())
                                    {
                                        if (body())
                                        {
                                            return true;
                                        }
                                    }
                                }
                            }

                           
                        }
                    }

                
                }

            }

            return false;
        }

        public bool dec_a2z()
        {

            if (Dec())
            {
                return true;
            }
            else
            {
                foreach (string s in dec_a2zSET)
                {
                    if (s == onlyClassPart[couter].ToString())
                        return true;
                }
            }

            return false;
        }

        public bool for1()
        {
            if (onlyClassPart[couter].ToString() == "ID")
            {
                couter++;
                if (LF_for2())
                {
                    return true;
                }
            }

            if (onlyClassPart[couter].ToString() == "INC" || onlyClassPart[couter].ToString() == "DEC")
            {
                couter++;
                if (onlyClassPart[couter].ToString() == "ID")
                {
                    couter++;
                    return true;
                }
            }

            return false;
        }

        public bool LF_for2()
        {
            if (AOP())
            {
                if (LF_for1())
                {
                    return true;
                }
            }

            if (LF_INC())
            {
                return true;
            }

            return false;
        }

        public bool LF_for1()
        {
            if (onlyClassPart[couter].ToString() == "ID")
            {
                couter++;
                if (for2())
                {
                    return true;
                }
            }

            if (constant())
            {
                if (for2())
                {
                    return true;
                }

              
            }

            return false;
        }


        public bool for2()
        {
            if (onlyClassPart[couter].ToString() == ",")
            {
                couter++;
                if (for1())
                {
                    return true;
                }
            }
            else
            {
                foreach (string s in for2SET)
                {
                    if (s == onlyClassPart[couter].ToString())
                        return true;
                }
            }

            return false;
        }


        public bool or_e_a2z()
        {
            if (or_e())
            {
                return true;
            }
            else
            {
                foreach (string s in or_e_a2zSET)
                {
                    if (s == onlyClassPart[couter].ToString())
                        return true;
                }
            }

            return false;
        }


        public bool agar()
        {
            if (onlyClassPart[couter].ToString() == "agar")
            {
                couter++;
                if (onlyClassPart[couter].ToString() == ":")
                {
                    couter++;
                    if (Agar_cond())
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        public bool Agar_cond()
        {
            if (or_e())
            {
                if (body())
                {
                    if (aurAgar())
                    {
                        return true;
                    }
                  
                }
            }
            return false;
        }

        public bool or_e()
        {
            if (and_e())
            {
                if (or_e1())
                {
                    return true;
                }
            }
            return false;
        }

        public bool or_e1()
        {
            if (onlyClassPart[couter].ToString() == "OR")
            {
                couter++;
                if (and_e())
                {
                    if (or_e1())
                    {
                        return true;
                    }
                }
            }
            else
            {
                foreach (string s in or_e1SET)
                {
                    if (s == onlyClassPart[couter].ToString())
                        return true;
                }
            }
            

            return false;
        }

        public bool and_e()
        {
            if (R_E())
            {
                if (and_e1())
                {
                    return true;
                }
            }

            return false;
        }

        public bool and_e1()
        {
            if (onlyClassPart[couter].ToString() == "AND")
            {
                couter++;
                if (R_E())
                {
                    if (and_e1())
                    {
                        return true;
                    }
                }
            }
            else
            {
                foreach (string s in and_e1SET)
                {
                    if (s == onlyClassPart[couter].ToString())
                        return true;
                }
            }

            return false;
        }

        public bool R_E()
        {
            if (E())
            {
                if (R_E1())
                {
                    return true;
                }
            
            }
           
            return false;
        }

        public bool R_E1()
        {
            if (ROP())
            {
                if (E())
                {
                    if (R_E1())
                    {
                        return true;
                    }
                }
            }
            else
            {
                foreach (string s in R_E1SET)
                {
                    if (s == onlyClassPart[couter].ToString())
                        return true;
                }
            }
            return false;
        }


        public bool ROP()
        {
            if (onlyClassPart[couter].ToString() == "==" || onlyClassPart[couter].ToString() == "<" || onlyClassPart[couter].ToString() == ">" || onlyClassPart[couter].ToString() == ">=" || onlyClassPart[couter].ToString() == "<=")
            {
                couter++;
                return true;
            }
            return false;
        }

        public bool E()
        {
            if (T())
            {
                if (E1())
                {
                    return true;
                }
            }
            return false;
        }

        public bool E1()
        {
            if (PM())
            {
                if (T())
                {
                    if (E1())
                    {
                        return true;
                    }
                }
            }else
             {
                 foreach (string s in E1SET)
                 {
                     if (s == onlyClassPart[couter].ToString())
                         return true;
                 }
             }
            return false;
        }
        public bool PM()
        {
            if (onlyClassPart[couter].ToString() == "+" || onlyClassPart[couter].ToString() == "-")
            {
                couter++;
                return true;
            }

            return false;
        }


        public bool T()
        {
            if (F())
            {
                if (T1())
                {
                    return true;
                }
            }
            return false;
        }

        public bool T1()
        {
            if (M_DM())
            {
                if (F())
                {
                    if (T1())
                    {
                        return true;
                    }
                }
            }

            else
            {
                foreach (string s in T1SET)
                {
                    if (s == onlyClassPart[couter].ToString())
                        return true;
                }
            }

            return false;
        }

        public bool M_DM()
        {

            if (onlyClassPart[couter].ToString() == "*" || onlyClassPart[couter].ToString() == "/" || onlyClassPart[couter].ToString() == "%")
            {
                couter++;
                return true;
            }
            return false;
        }

        public bool F()
        {
            if (onlyClassPart[couter].ToString() == "ID")
            {
                couter++;
                if (LF_F())
                {
                    return true;
                }
            
            }
            else if (onlyClassPart[couter].ToString() == "NOT")
            {
                couter++;
                if (F())
                {
                    return true;
                }
            }
            else
                if (onlyClassPart[couter].ToString() == "int_constant" || onlyClassPart[couter].ToString() == "char_constant"
                    || onlyClassPart[couter].ToString() == "string_constant" || onlyClassPart[couter].ToString() == "float_constant"
                    )
                {
                    couter++;
                    return true;
                }
                else if (onlyClassPart[couter].ToString() == "(")
                {
                    couter++;
                    if (OR_E())
                    {
                        if (onlyClassPart[couter].ToString() == ")")
                        {
                            couter++;
                            return true;
                        }
                    }
                }
                else if (onlyClassPart[couter].ToString() == "INC")
                {
                    couter++;
                    if (onlyClassPart[couter].ToString() == "ID")
                    {
                        couter++;
                        return true;
                    }
                }
                else if (onlyClassPart[couter].ToString() == "DEC")
                {
                    couter++;
                    if (onlyClassPart[couter].ToString() == "ID")
                    {
                        couter++;
                        return true;
                    }
                }
                else if (DT())
                {
                    if (onlyClassPart[couter].ToString() == "ID")
                    {
                        couter++;
                        if (Init())
                        {
                            if (LISTed())
                            {
                                return true;
                            }
                       
                        }
                    }
                }

            return false;
        }

        public bool OR_E()
        {
            if (and_e())
            {
                if (or_e1())
                {
                    return true;
                }
            }
            return false;
        }

        public bool LISTed()
        {
            if (onlyClassPart[couter].ToString() == ";")
            {
                couter++;
                if (Dec())
                {
                    return true;
                }
            }
            if (onlyClassPart[couter].ToString() == ",")
            {
                couter++;
                if (onlyClassPart[couter].ToString() == "ID")
                {
                    couter++;
                    if (Init())
                    {
                        if (LISTed())
                        {
                            return true;
                        }
                    }
                }
            }
            else
            {
                foreach (string s in LISTedSET)
                {
                    if (s == onlyClassPart[couter].ToString())
                        return true;
                }
            }


            return false;
        }

        public bool Dec()
        {
            if (DT())
            {
                if (onlyClassPart[couter].ToString() == "ID")
                {
                    couter++;
                    if (Init())
                    {
                        if (LISTed())
                        {
                            return true;
                        }
                    }
               
                }
            }
            return false;
        }
        public bool Init()
        {

            if (onlyClassPart[couter].ToString() == "=")
            {
                couter++;
                if (LF_Init())
                {
                    return true;
                }
            }
            else
            {
                foreach (string s in InitSET)
                {
                    if (s == onlyClassPart[couter].ToString())
                        return true;
                }
            }


            return false;
        }

        public bool LF_Init()
        {
            if (onlyClassPart[couter].ToString() == "ID")
            {
                couter++;
                if (LF1_Init())
                {
                    return true;
                }
            }
             if (constant())
            {
                if (forAirth())
                return true;
            }

            return false;
        }

        bool forAirth()
        {
            if(PM()||M_DM())
            {
                if(LFforAirth())
                {
                    return true;
                }
            }

            else
            {
                foreach (string s in forAirthSET)
                {
                    if (s == onlyClassPart[couter].ToString())
                        return true;
                }
            }
            return false;
        }

        bool LFforAirth()
        {
            if (onlyClassPart[couter].ToString() == "ID")
            {
                couter++;
                if(forAirth())
                {
                    return true;
                }
            }
            if(constant())
            {
                if(forAirth())
                {
                    return true;
                }
            }

            return false;
        }

        public bool LF1_Init()
        {
            if (Init())
            {
                return true;
            }

            if (onlyClassPart[couter].ToString() == ":")
            {
                couter++;
                if (param())
                {
                    return true;
                }
            }

            return false;
        }

        public bool LF_F()
        {
            if (onlyClassPart[couter].ToString() == ":")
            {
                couter++;
                if (param())
                {
                    return true;
                }

            }
             if (onlyClassPart[couter].ToString() == "ID")
            {
                couter++;
                if (LF_INC())
                {
                    return true;
                }
            }
             if (onlyClassPart[couter].ToString() == "INC" || onlyClassPart[couter].ToString() == "DEC")
            {
                couter++;
                if (LF_F1())
                {
                    return true;
                }

            }
             else
             {
                 foreach (string s in LF_FSET)
                 {
                     if (s == onlyClassPart[couter].ToString())
                     {
                         return true;
                     }
                         
                 }
             }



            return false;
        }

        public bool LF_F1()
        {
            if (onlyClassPart[couter].ToString() == "ID")
            {
                couter++;
                return true;
            }

            else
            {
                foreach (string s in LF_F1SET)
                {
                    if (s == onlyClassPart[couter].ToString())
                        return true;
                }
            }
            return false;
        }


        public bool body()
        {
            if (s_st())
            {
                return true;
            }
            if (onlyClassPart[couter].ToString() == "{")
            {
                couter++;
                if (m_st())
                {
                    if (onlyClassPart[couter].ToString() == "}")
                    {
                        couter++;
                        return true;
                    }

                }
            }

            return false;
        }

        public bool aurAgar()
        {
            if (onlyClassPart[couter].ToString() == "aurAgar")
            {
                couter++;
                if (onlyClassPart[couter].ToString() == ":")
                {
                    couter++;
                    if (onlyClassPart[couter].ToString() == "Agar_cond")
                    {
                        couter++;

                        return true;

                    }


                }


            }

            else
            {
                foreach (string s in aurAgarSET)
                {
                    if (s == onlyClassPart[couter].ToString())
                        return true;
                }
            }
            return false;
        }

        public bool LF_s_st()
        {
            if (onlyClassPart[couter].ToString() == ":")
            {
              
                couter++;
                if (param())
                {
                    return true;
                }
            }
             if (LF_INC()) return true;
            return false;
        }

        public bool LF_INC()
        {
            if (onlyClassPart[couter].ToString() == "INC")
            {
                couter++;
                return true;
            }
            else if (onlyClassPart[couter].ToString() == "INC")
            {
                couter++;
                return true;
            }

            return false;
        }

        public bool param()
        {
            if (onlyClassPart[couter].ToString() == "ID")
            {
                couter++;
               
                if (LF2_param())
                {
                    return true;
                }
            }

            if (array())
            {
                if (param2())
                {
                    return true;
                }
            }

            if (constant())
            {
                if (param2())
                {
                    return true;
                }
            }

            return false;
        }

        public bool LF2_param()
        {
            if (param2())
            {
                return true;
            }

            if (AOP())
            {
                if (LF_param())
                {
                    return true;
                }
            }

            if (onlyClassPart[couter].ToString() == ":")
            {
                couter++;
                if (param())
                {
                    if (param2())
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public bool param2()
        {
            if (onlyClassPart[couter].ToString() == ",")
            {
                couter++;
                if (param())
                {
                    return true;
                }
            }
            else
            {
                foreach (string s in param2SET)
                {
                    if (s == onlyClassPart[couter].ToString())
                        return true;
                }
            }


            return false;
        }

        public bool LF_param()
        {
            if (onlyClassPart[couter].ToString() == "ID")
            {
                couter++;
                if (param2())
                {
                    return true;
                }
            }

            if (constant())
            {
                if (param2())
                {
                    return true;
                }
            }

            return false;
        }

        public bool constant()
        {
            if (onlyClassPart[couter].ToString() == "int_constant" || onlyClassPart[couter].ToString() == "char_constant"
                || onlyClassPart[couter].ToString() == "string_constant" || onlyClassPart[couter].ToString() == "float_constant"
                )
            {
                couter++;
                return true;
            }

            return false;
        }

        public bool AOP()
        {
            if (onlyClassPart[couter].ToString() == "+" || onlyClassPart[couter].ToString() == "-"
                || onlyClassPart[couter].ToString() == "*" || onlyClassPart[couter].ToString() == "/")
            {
                couter++;
                return true;
            }
            return false;
        }

        public bool array()
        {
            if (DT())
            {
                if (onlyClassPart[couter].ToString() == "ID")
                {
                    couter++;
                    if (onlyClassPart[couter].ToString() == ".=")
                    {
                        couter++;
                        if (onlyClassPart[couter].ToString() == "{")
                        {
                            couter++;
                            if (LF_array())
                            {
                                return true;
                            }
                        }
                    }
                }
            }

            return false;
        }

        public bool DT()
        {
            if (onlyClassPart[couter].ToString() == "int" || onlyClassPart[couter].ToString() == "float" ||
                onlyClassPart[couter].ToString() == "char" || onlyClassPart[couter].ToString() == "string"
                )
            {
                couter++;
                return true;
            }
            return false;
        }
        public bool LF_array()
        {
            if (ar_T())
            {
                if (onlyClassPart[couter].ToString() == "}")
                {
                    couter++;
                    return true;
                }
            }

            if (Ar())
            {
                if (onlyClassPart[couter].ToString() == "}")
                {
                    couter++;
                    return true;
                }
            }
            else if (onlyClassPart[couter].ToString() == "}")
            {
                couter++;
                return true;
            }

            return false;
        }

        public bool Ar()
        {
            if (constant())
            {
                if (Ar2())
                {
                    return true;
                }

            }
            else
            {
                foreach (string s in ArSET)
                {
                    if (s == onlyClassPart[couter].ToString())
                        return true;
                }
            }
            return false;
        }

        public bool Ar2()
        {
            if (onlyClassPart[couter].ToString() == ",")
            {
                couter++;
                if (constant())
                {
                    if (Ar())
                    {
                        return true;
                    }
                }
            }

            else
            {
                foreach (string s in Ar2SET)
                {
                    if (s == onlyClassPart[couter].ToString())
                        return true;
                }
            }
            return false;
        }

        public bool ar_T()
        {
            if (onlyClassPart[couter].ToString() == "(")
            {
                couter++;
                if (LF_ar_T())
                {
                    return true;
                }
            }
            return false;
        }

        public bool LF_ar_T()
        {
            if (constant())
            {
                if (onlyClassPart[couter].ToString() == ",")
                {
                    couter++;
                    if (constant())
                    {
                        if (onlyClassPart[couter].ToString() == ")")
                        {
                            couter++;
                            if (ar2d())
                            {
                                return true;
                            }
                          
                        }
                    }
                }
            }

            if (onlyClassPart[couter].ToString() == ",")
            {
                couter++;
                if (onlyClassPart[couter].ToString() == ")")
                {
                    couter++;
                    return true;
                }
            }

            return false;
        }

        public bool ar2d()
        {
            if (onlyClassPart[couter].ToString() == ",")
            {
                couter++;
                if (array2d_body())
                {
                    return true;
                }
            }

            else
            {
                foreach (string s in ar2dSET)
                {
                    if (s == onlyClassPart[couter].ToString())
                        return true;
                }
            }
            return false;
        }

        public bool array2d_body()
        {
            if (constant())
            {
                if (onlyClassPart[couter].ToString() == ",")
                {
                    couter++;
                    if (constant())
                    {
                        if (onlyClassPart[couter].ToString() == ")")
                        {
                            couter++;
                            if (ar2d())
                            {
                                return true;
                            }
                      
                        }
                    }
                }
            }

            return false;
        }

    }
}