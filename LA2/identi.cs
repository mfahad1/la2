﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LA2
{
    class identi
    {
        public bool identi1(string toIdentify)
        { 

            int[,] idArr = new int[,]
            {
                {1,3,2},
                {1,1,1},
                {1,1,2},
                {3,3,3}
            };

            int currSt = 0, i = 0;
            int strLen = toIdentify.Length;
            while (i < strLen)
            {
                if ((toIdentify[i] >= 65 && toIdentify[i] <= 90) || (toIdentify[i] >= 97 && toIdentify[i] <= 122))
                    currSt = idArr[currSt, 0];

                else if ((toIdentify[i] >= 48 && toIdentify[i] <= 57))
                    currSt = idArr[currSt, 1];

                else if (toIdentify[i] == 95)
                    currSt = idArr[currSt, 2];

                i++;
            }

            if (currSt == 1)
            {
                // Console.WriteLine(toIdentify + "   Identifier");
                return true;
            }
            else return false;
        }

    }

}
